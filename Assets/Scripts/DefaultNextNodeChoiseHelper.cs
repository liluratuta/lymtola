using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultNextNodeChoiseHelper : INextNodeChoiseHelper
{
    public Node Choise(Node currentNode)
    {
        var directions = System.Enum.GetValues(typeof(CardinalDirection));
        List<CardinalDirection> preferredDirections = new List<CardinalDirection>();
        float minCost = float.MaxValue;

        foreach (CardinalDirection direction in directions)
        {
            float cost = currentNode.GetGoalDistance(direction) + currentNode.GetDanger(direction);

            if (cost < minCost)
            {
                minCost = cost;
            }

            if (cost == minCost)
            {
                preferredDirections.Add(direction);
            }
        }

        var choisedDirectionCount = preferredDirections.Count;

        if (choisedDirectionCount == 1)
        {
            return currentNode.GetAdjacent(preferredDirections[0]);
        }

        if (choisedDirectionCount > 1)
        {
            var index = Random.Range(0, choisedDirectionCount);

            return currentNode.GetAdjacent(preferredDirections[index]);
        }

        var randomIndex = Random.Range(0, 4);

        var randomDirecton = (CardinalDirection)(directions.GetValue(randomIndex));

        return currentNode.GetAdjacent(randomDirecton);
    }
}
