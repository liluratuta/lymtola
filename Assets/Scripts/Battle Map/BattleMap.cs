using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMap : MonoBehaviour, IRoutable
{
    [SerializeField] private Vector2Int size;
    [SerializeField] private List<Vector2Int> attractionPoints = new List<Vector2Int>();

    private GridConverter gridConverter;
    private DistanceMatrix distanceMatrix = new DistanceMatrix();

    private int[,,] matrix;

    private Vector2Int[] directions = new Vector2Int[4]
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    private void Awake()
    {
        gridConverter = new GridConverter(Vector3.zero, 2f);

        FillMatrix();
    }

    public Vector3 GetNextPoint(Vector3 currentPoint)
    {
        var gridPosition = gridConverter.ConvertToGrid(currentPoint);

        if (CheckGridPosition(gridPosition) == false)
        {
            return gridConverter.ConvertToWorld(gridPosition);
        }

        int minDirection = GetMinDirection(gridPosition);

        return gridConverter.ConvertToWorld(gridPosition + directions[minDirection]);
    }

    private void FillMatrix()
    {
        for (var x = 0; x < size.x; x++)
        {
            for (var y = 0; y < size.y; y++)
            {
                matrix[x, y, 0] = int.MaxValue;
                matrix[x, y, 1] = int.MaxValue;
                matrix[x, y, 2] = int.MaxValue;
                matrix[x, y, 3] = int.MaxValue;
            }
        }

        distanceMatrix.Fill(attractionPoints, size, ref matrix);
    }

    private bool CheckGridPosition(Vector2Int position)
    {
        if (position.x < 0 || position.x >= size.x)
        {
            return false;
        }
        if (position.y < 0 || position.y >= size.y)
        {
            return false;
        }

        return true;
    }

    private int GetMinDirection(Vector2Int position)
    {
        return Mathf.Min(
                Mathf.Min(matrix[position.x, position.y, 0], matrix[position.x, position.y, 1]),
                Mathf.Min(matrix[position.x, position.y, 2], matrix[position.x, position.y, 3])
               );
    }
}
