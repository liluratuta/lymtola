using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class TileView : MonoBehaviour
{
    private Material material;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    public Vector3 Position
    { 
        set
        {
            transform.position = value;
        }
    }
    
    public float ColorValue
    { 
        set
        {
            material.color = new Color(value, value, value);
        }
    }

}
