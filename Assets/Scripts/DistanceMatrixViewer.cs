using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceMatrixViewer : MonoBehaviour
{
    [SerializeField] private Vector2Int matrixSize;
    [SerializeField] private List<Vector2Int> startPoints = new List<Vector2Int>();
    [SerializeField] private GridView gridView;
    private DistanceMatrix distanceMatrix;
    private int[,,] matrix;

    private void Awake()
    {
        matrix = new int[matrixSize.x, matrixSize.y, 4];

        distanceMatrix = new DistanceMatrix();

        gridView.CreateGrid(matrixSize);
    }

    private void Start()
    {
        for (var x = 0; x < matrixSize.x; x++)
        {
            for (var y = 0; y < matrixSize.y; y++)
            {
                matrix[x, y, 0] = int.MaxValue;
                matrix[x, y, 1] = int.MaxValue;
                matrix[x, y, 2] = int.MaxValue;
                matrix[x, y, 3] = int.MaxValue;
            }
        }

        distanceMatrix.Fill(startPoints, matrixSize, ref matrix);

        float maxDistance = Mathf.Max(matrixSize.x, matrixSize.y);

        for (var x = 0; x < matrixSize.x; x++)
        {
            for (var y = 0; y < matrixSize.y; y++)
            {
                Debug.Log($"({x},{y}) = {GetMinDistance(x, y)}");
                gridView.ChangeTile(new Vector2Int(x, y), (float)GetMinDistance(x, y) / maxDistance);
            }
        }
    }

    private int GetMinDistance(int x, int y)
    {
        return Mathf.Min(
                Mathf.Min(matrix[x, y, 0], matrix[x, y, 1]),
                Mathf.Min(matrix[x, y, 2], matrix[x, y, 3])
               );
    }
}
