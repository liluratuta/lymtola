using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPathFinder
{
    List<Vector2Int> Find(Vector2Int start, Vector2Int end);
}
