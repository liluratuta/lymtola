﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu]
public class EnemyFactory : GameObjectFactory
{
    [SerializeField] private Enemy _enemyPrefab;

    public Enemy Get()
    {
        return Create(_enemyPrefab);
    }
}