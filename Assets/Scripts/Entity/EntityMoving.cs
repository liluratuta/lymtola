using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityMoving : MonoBehaviour
{
    private List<Vector3> path;

    [SerializeField] private Transform target;

    private int currentPointIndex = 0;
    private float distanceToCurrentPoint => Vector3.Distance(transform.position, path[currentPointIndex]);

    private void Update()
    {
        var angle = Vector3.SignedAngle(transform.forward, target.forward, Vector3.up);

        var deviationDegree = Mathf.InverseLerp(0, 180, Mathf.Abs(angle));

        transform.Rotate(Vector3.up, 5 * deviationDegree * Mathf.Sign(angle));
    }

    private float GetDisplacedAngleToPoint(int pointIndex)
    {
        var width = (path[pointIndex + 1] - path[pointIndex]).magnitude;
        var adjacentDirection = (path[pointIndex + 1] - path[pointIndex - 1]).normalized;
        Vector3 displaceDirection = Vector3.zero;

        displaceDirection.x = adjacentDirection.z;
        displaceDirection.z = -adjacentDirection.x;

        var direction = path[pointIndex] + (displaceDirection * width * 0.5f);

        return Vector3.SignedAngle(transform.forward, direction, Vector3.up);
    }
}
