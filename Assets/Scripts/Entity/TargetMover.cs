using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TargetMover : MonoBehaviour
{
    public event Action TargetReached;

    private new Transform transform;

    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;

    private Vector3 targetPosition;

    private bool IsTargetReached
    {
        get
        {
            var difference = targetPosition - transform.position;
            difference.y = 0;
            return difference.sqrMagnitude < 1f;
        }
    }

    private void Awake()
    {
        transform = GetComponent<Transform>();

        targetPosition = transform.position + transform.forward * 5;
    }

    private void Update()
    {
        Rotate();
        Move();

        if (IsTargetReached)
        {
            TargetReached?.Invoke();
        }
    }

    public void UpdateTarget(Vector3 newTarget)
    {
        targetPosition = newTarget;
    }

    private void Rotate()
    {
        var angle = Vector3.SignedAngle(transform.forward, targetPosition - transform.position, Vector3.up);

        var deviationDegree = Mathf.Sign(angle) * Mathf.InverseLerp(0, 180, Mathf.Abs(angle));

        transform.Rotate(Vector3.up, rotationSpeed * deviationDegree * Time.deltaTime);
    }

    private void Move()
    {
        transform.position = transform.position + transform.forward * speed * Time.deltaTime;
    }
}
