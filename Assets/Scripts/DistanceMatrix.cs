using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceMatrix
{
    private Queue<BypassData> bypassElements = new Queue<BypassData>();
    
    private HashSet<Vector2Int> passedPoints = new HashSet<Vector2Int>();
    
    private Vector2Int[] directions = new Vector2Int[4]
    {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };

    private struct BypassData
    {
        public Vector2Int point;
        public int distance;
        public int direction;

        public BypassData(Vector2Int point, int distance, int direction)
        {
            this.point = point;
            this.distance = distance;
            this.direction = direction;
        }
    }

    public void Fill(List<Vector2Int> starts, Vector2Int size, ref int[,,] matrix)
    {
        BypassData currentElement;
        Vector2Int currentPoint;
        Vector2Int nextPoint;

        foreach (var start in starts)
        {
            bypassElements.Enqueue(new BypassData(start, 0, 0));
        }

        while (bypassElements.Count > 0)
        {
            currentElement = bypassElements.Dequeue();
            currentPoint = currentElement.point;

            if (matrix[currentPoint.x, currentPoint.y, currentElement.direction] > currentElement.distance)
            {
                matrix[currentPoint.x, currentPoint.y, currentElement.direction] = currentElement.distance;
            }

            if (passedPoints.Contains(currentPoint))
            {
                continue;
            }

            for (var i = 0; i < 4; i++)
            {
                nextPoint = currentPoint + directions[i];

                if (CheckOverseas(nextPoint, size))
                {
                    continue;
                }
                
                bypassElements.Enqueue(
                    new BypassData(
                        nextPoint,
                        currentElement.distance + 1,
                        ReverseDirection(i))
                    );
            }

            passedPoints.Add(currentPoint);
        }

        bypassElements.Clear();
        passedPoints.Clear();
    }

    private int ReverseDirection(int direction)
    {
        return (direction + 2) % 4;
    }

    private bool CheckOverseas(Vector2Int point, Vector2Int borderSize)
    {
        if (point.x < 0 || point.x >= borderSize.x)
        {
            return true;
        }

        if (point.y < 0 || point.y >= borderSize.y)
        {
            return true;
        }

        return false;
    }
}
